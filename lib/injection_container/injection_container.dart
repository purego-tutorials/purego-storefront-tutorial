import 'package:core/logger/logger.dart';
import 'package:core/logger/logger_impl.dart';
import 'package:core/service_locator.dart';
import 'package:get_it/get_it.dart';

part 'application_injectables.dart';
part 'core_injectables.dart';
part 'dependency_injectables.dart';
part 'service_injectables.dart';

Future<void> initialize() async {
  await Future.wait([
    injectServices(serviceLocator),
    injectionApplications(serviceLocator),
    injectCore(serviceLocator),
    injectDependencies(serviceLocator),
  ]);
}
