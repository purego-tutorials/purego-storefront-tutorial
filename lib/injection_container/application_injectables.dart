part of 'injection_container.dart';

/// Registers Blocs to the [serviceLocator].
///
/// All blocs must be injected via the [GetIt.registerFactory].
///
/// NOTE: Sort injectables alphabetically.
Future<void> injectionApplications(GetIt serviceLocator) async {
  // AAA
  // BBB
  // CCC
  // DDD
  // EEE
  // FFF
  // GGG
  // HHH
  // III
  // JJJ
  // KKK
  // LLL
  // MMM
  // NNN
  // OOO
  // PPP
  // QQQ
  // RRR
  // SSS
  // TTT
  // UUU
  // VVV
  // WWW
  // XXX
  // YYY
  // ZZZ
}
