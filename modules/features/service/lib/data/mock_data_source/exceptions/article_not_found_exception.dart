/// An exception thrown when an article is not found.
class ArticleNotFoundException implements Exception {}
