part of 'runner_bloc.dart';

abstract class RunnerState with EquatableMixin {
  const RunnerState();
}

class RunnerInitial extends RunnerState {
  const RunnerInitial();

  @override
  List<Object?> get props => [];
}

class Running<Params> extends RunnerState {
  const Running({required this.params, required this.runToken});

  final Params params;
  final int runToken;

  @override
  List<Object?> get props => [params, runToken];
}

class RunFailed<Params> extends RunnerState {
  const RunFailed({
    required this.params,
    required this.runToken,
    required this.failure,
  });

  final Params params;

  final int runToken;
  final Failure failure;

  @override
  List<Object?> get props => [failure, params, runToken];
}

class RunSuccess<ReturnType> extends RunnerState {
  const RunSuccess({required this.runToken, required this.result});

  final int runToken;
  final ReturnType result;

  @override
  List<Object?> get props => [runToken, result];
}
