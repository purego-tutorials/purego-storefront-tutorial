part of 'runner_bloc.dart';

abstract class RunnerEvent with EquatableMixin {
  const RunnerEvent();
}

class Run<Params> extends RunnerEvent {
  const Run({required this.params});

  final Params params;

  @override
  List<Object?> get props => [params];
}
