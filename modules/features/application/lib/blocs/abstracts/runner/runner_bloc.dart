import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/failures/abstracts/failure.dart';
import 'package:equatable/equatable.dart';

part 'runner_event.dart';
part 'runner_state.dart';

/// A generic bloc for executing tasks that loads asynchrnously, then either
/// returns a [Failure] or a specified [Result].
///
/// [Params] represents the parameter objected needed for [task].
/// [ReturnType] signifies the return value of [task] if no [Failure] occurs.
///
/// This handles events as a single stream, whereas [RunnerCubit] events are
/// handled on their own method call.
///
/// e.g.
/// ```dart
/// Run event
/// Run event
///
/// Running state
/// RunSuccess state
/// Running state
/// RunSuccess state
/// ```
abstract class RunnerBloc<Params, ReturnType>
    extends Bloc<RunnerEvent, RunnerState> {
  RunnerBloc() : super(const RunnerInitial());

  ReturnType? lastResult;

  Future<Either<Failure, ReturnType>> task({required Params params});

  /// Serves as an identifier for each [_onRun] call.
  ///
  /// Every time [_onRun] is called, this gets incremented.
  /// TODO: convert to private variable
  int _runToken = 0;

  int get runToken => _runToken;

  @override
  Stream<RunnerState> mapEventToState(RunnerEvent event) async* {
    if (event is Run<Params>) {
      yield* _onRun(event);
    }
  }

  Stream<RunnerState> _onRun(Run<Params> event) async* {
    final runToken = ++_runToken;

    yield Running<Params>(params: event.params, runToken: runToken);

    final result = await task(params: event.params);

    yield result.fold(
      (l) => RunFailed<Params>(
          params: event.params, runToken: runToken, failure: l),
      (r) => RunSuccess<ReturnType>(runToken: runToken, result: lastResult = r),
    );
  }
}
