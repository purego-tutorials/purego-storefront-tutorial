import 'package:flutter/material.dart';

class ArticleListPage extends StatelessWidget {
  const ArticleListPage({Key? key}) : super(key: key);

  static const _articleIds = [
    "XbOOmWsB8U",
    "R5igwtmtfJ",
    "NON_EXISTING_ARTICLE_ID"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              for (final articleId in _articleIds)
                SizedBox(
                  width: double.maxFinite,
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text('Article $articleId'),
                  ),
                ),
            ],
          )),
    );
  }
}
