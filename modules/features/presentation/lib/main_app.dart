import 'package:flutter/material.dart';
import 'package:presentation/pages/article_list_page.dart';

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: ArticleListPage());
  }
}
